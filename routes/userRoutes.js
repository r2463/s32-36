const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePw,
	adminStatus,
	userStatus,
	deleteUser,
	enroll

	
} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		return res.status(500).json(err)
	}
})


//REGISTER A USER 
router.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



//LOGIN THE USER
	// authentication
router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// UPDATE USER INFORMATION
router.put('/update', verify, async (req, res) =>{

	// console.log(decode(req.headers.authorization).id)
	const userId = decode(req.headers.authorization).id

	try{
		update(userId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



// UPDATE USER PASSWORD
router.patch('/update-password', verify, async (req, res) =>{

	// console.log(decode(req.headers.authorization).id)
	const userId = decode(req.headers.authorization).id

	try{
		updatePw(userId, req.body.password).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route to update user's isd Admin status to true. Make sure the route is secured.


// ADMIN ACCESS
router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})


// Create a route to update user's isd Admin status to false. Make sure the route is secured.

// USER ACCESS
router.patch('/isUser', verifyAdmin, async (req, res) =>{

	try{
		await userStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}


})


//Create a route to delete a user from the DB, make sure the route is secured
	// Stretch goal, only admin can delete a user

router.delete('/delete-user', verifyAdmin, async (req, res) =>{

	try{
		await deleteUser(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}


})



//Create a route to enroll a user
router.post('/enroll', verify, async (req, res) =>{
	const user = decode(req.headers.authorization).isAdmin

	const data = {
		userId: decode(req.headers.authorization).id,
		courseId: req.body.courseId
		
	}

	if(user == false){

		try{
			await enroll(data).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
		
	}
	else{
		res.send(`Only with user access can enroll`)
	}
})


//Export the router module to be used in index.js file
module.exports = router;
